# PacHost

Easily set up your own Repository with prebuilt AUR packages.

## Dependencies

- `pacman` – providing makepkg and repo-add
- `coreutils` – cat, head, tail and so on
- `git` – for updating the AUR packages

## Demo

a demonstration can be found at [pachost.moritz.sh](https://pachost.moritz.sh)

to add this to your repositories and try it out simply execute:

```
curl -O https://pachost.moritz.sh/pubkey.gpg && sudo pacman-key --add pubkey.gpg && sudo pacman-key --lsign-key A602DD01FB9653C7EDAAC572D786E1EED2C71596 && rm sublimehq-pub.gpg && echo -e "\n[mp-pachost]\nServer = https://pachost.moritz.sh/" | sudo tee -a /etc/pacman.conf
```

## ToDo

- build in chroot
- write a dockerfile
- provide AUR package
- ~~setup demo~~
- document usage
- prevent simultaneous execution
- PHP frontend
