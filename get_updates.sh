#!/bin/env bash

source '/etc/pachost.conf'

cd "$ph_REPODIR";
for repo in */; do
	repoName=$(basename "$repo")
	echo -n "checking Version of $repoName..."
	currentVersion=$(cat "$repoName.hash" 2> /dev/null)
	cd "$repo";
	
	git pull > /dev/null 2>&1
	remoteVersion=$("../$repoName.customGetVersion.sh" 2>/dev/null || git describe --always)

	if [[ $remoteVersion != $currentVersion ]]; then
		echo -e "\033[0;31mupdate found\033[0m"

		"$ph_SCRIPTDIR/build_update.sh"
		if [[ $? == 0 ]]; then

			# uncomment to remove BUILDINFO from created package
			#pkgfile=$(env ls -Art "$ph_SERVEDIR" | env tail -n 2 | env head -n 1)
			#zstdcat "$pkgfile" | tar --delete .BUILDINFO | zstd - -fo "$pkgfile"

			"$ph_SCRIPTDIR/update_index.sh"
			if [[ $? == 0 ]]; then
				echo "$remoteVersion" > "../$repoName.hash"
			else
				echo "failed to update database!"
			fi
		fi
	else
		echo -e "\033[0;32mno update\033[0m"
	fi

	cd ..
done
