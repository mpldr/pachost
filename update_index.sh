#!/bin/env bash

source '/etc/makepkg.conf'
source '/etc/pachost.conf'

env find "$ph_SERVEPATH" -type f -regex ".*\.zst" -exec repo-add -s --key "$ph_SIGNKEY" "$ph_SERVEPATH/$ph_REPONAME.db.$ph_DBEXTENSION" "{}" "+"
